class Event < ActiveRecord::Base
  has_many :jobs, through: :event_job_period_associations
  has_many :periods, through: :event_job_period_associations
  has_many :event_job_period_associations, dependent: :destroy

end
