class Job < ActiveRecord::Base
  has_many :job_job_associations, foreign_key: "successor_id", :dependent => :destroy
  has_many :reverse_job_associations, foreign_key: "predecessor_id", class_name: "JobJobAssociation", :dependent => :destroy
  has_many :successors, through: :job_job_associations, source: :successor_id
  has_many :predecessors, through: :reverse_job_associations, source: :predecessor_id
  has_many :events, through: :event_job_period_associations
  has_many :periods, through: :event_job_period_associations

end


#def ressource_demand
#  if ressource_demand < 0.integer
#    errors.add(:integer, "should be greater than or equal to 0")
#  end


#class Job < ApplicationRecord
#validates :points, numericality: true
#validates :ressource_demand, numericality: { greater_than: 0 }
#end


#integer_field_tag 'ressource_demand', nil, min: 1
# => <input id="quantity" name="quantity" min="1" type="number" />