class JobJobAssociation < ActiveRecord::Base
  belongs_to :successor, class_name: "Job"
  belongs_to :predecessor, class_name: "Job"
  validates :successor_id, presence: true
  validates :predecessor_id, presence: true
end

