json.array!(@job_job_associations) do |job_job_association|
  json.extract! job_job_association, :id, :predecessor_id, :successor_id
  json.url job_job_association_url(job_job_association, format: :json)
end
