json.array!(@jobs) do |job|
  json.extract! job, :id, :name, :processing_time, :ressources_demand
  json.url job_url(job, format: :json)
end
