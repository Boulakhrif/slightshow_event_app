class JobJobAssociationsController < ApplicationController
  before_action :set_job_job_association, only: [:show, :edit, :update, :destroy]

  # GET /job_job_associations
  # GET /job_job_associations.json
  def index
    @job_job_associations = JobJobAssociation.all
  end

  # GET /job_job_associations/1
  # GET /job_job_associations/1.json
  def show

  end

  # GET /job_job_associations/new
  def new
    @job_job_association = JobJobAssociation.new
  end

  # GET /job_job_associations/1/edit
  def edit
  end

  # POST /job_job_associations
  # POST /job_job_associations.json
  def create
    @job_job_association = JobJobAssociation.new(job_job_association_params)

    respond_to do |format|
      if @job_job_association.save
        format.html { redirect_to @job_job_association, notice: 'Die Vorgänger-Nachfolger-Beziehung wurde erfolgreich angelegt.' }
        format.json { render :show, status: :created, location: @job_job_association }
      else
        format.html { render :new }
        format.json { render json: @job_job_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_job_associations/1
  # PATCH/PUT /job_job_associations/1.json
  def update
    respond_to do |format|
      if @job_job_association.update(job_job_association_params)
        format.html { redirect_to @job_job_association, notice: 'Die Vorgänger-Nachfolger-Beziehung wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @job_job_association }
      else
        format.html { render :edit }
        format.json { render json: @job_job_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_job_associations/1
  # DELETE /job_job_associations/1.json
  def destroy
    @job_job_association.destroy
    respond_to do |format|
      format.html { redirect_to job_job_associations_url, notice: 'Die Vorgänger-Nachfolger-Beziehung wurde entfernt.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_job_association
      @job_job_association = JobJobAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_job_association_params
      params.require(:job_job_association).permit(:predecessor_id, :successor_id)
    end
end
