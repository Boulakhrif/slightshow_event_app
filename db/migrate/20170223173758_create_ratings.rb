class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.references :comment, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :score
      t.string :default
      t.string

      t.timestamps null: false
    end
  end
end
