class CreateEvents < ActiveRecord::Migration
  def change
    if (!ActiveRecord::Base.connection.tables.include?("events"))
    create_table :events do |t|
      t.string :name
      t.string :occasion
      t.integer :deadline

      t.timestamps null: false
    end
    end
    end
end
