User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

User.create!(name:  "Team EvenTraum",
             email: "team@eventraum.com",
             password:              "geheim",
             password_confirmation: "geheim",
             admin: true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

  users = User.order(:created_at).take(6)
  50.times do
    content = Faker::Lorem.sentence(5)
    users.each { |user| user.microposts.create!(content: content) }
    end


(0..15).each do |n|
  name = "t#{n}"
  Period.create!(name: name)
end


Event1 = Event.create!(name: "Hochzeit",          deadline: 7)
Event2 = Event.create!(name: "Betriebsfeier",       deadline: 14)
Event3 = Event.create!(name: "Geburtstag",   deadline: 12)

Vorgang1 = Job.create!(name: "1. Start",                 processing_time: 0,   ressources_demand: 0)
Vorgang2 = Job.create!(name: "2. Konzeptplanung",        processing_time: 3,   ressources_demand: 3)
Vorgang3 = Job.create!(name: "3. Buchung Entertainment", processing_time: 1,   ressources_demand: 1)
Vorgang4 = Job.create!(name: "4. Menü-/Büffetplanung",   processing_time: 1,   ressources_demand: 1)
Vorgang5 = Job.create!(name: "5. Floristen beauftragen", processing_time: 1,   ressources_demand: 1)
Vorgang6 = Job.create!(name: "6. Einkaufen",             processing_time: 2,   ressources_demand: 3)
Vorgang7 = Job.create!(name: "7. Ende",                  processing_time: 0,   ressources_demand: 0)


Vorgang1Vorgang1 = JobJobAssociation.create!(predecessor_id: 1, successor_id:2)
Vorgang2Vorgang3 = JobJobAssociation.create!(predecessor_id: 2, successor_id:3)
Vorgang3Vorgang4 = JobJobAssociation.create!(predecessor_id: 3, successor_id:4)
Vorgang3Vorgang5 = JobJobAssociation.create!(predecessor_id: 3, successor_id:5)
Vorgang4Vorgang6 = JobJobAssociation.create!(predecessor_id: 4, successor_id:6)
Vorgang5Vorgang6 = JobJobAssociation.create!(predecessor_id: 5, successor_id:6)
Vorgang6Vorgang7 = JobJobAssociation.create!(predecessor_id: 6, successor_id:7)


### Für Event 1
E1V1t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 1)
E1V2t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 1)
E1V3t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 1)
E1V4t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 1)
E1V5t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 1)
E1V6t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 1)
E1V7t1=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 1)

E1V1t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 2)
E1V2t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 2)
E1V3t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 2)
E1V4t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 2)
E1V5t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 2)
E1V6t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 2)
E1V7t2=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 2)

E1V1t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 3)
E1V2t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 3)
E1V3t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 3)
E1V4t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 3)
E1V5t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 3)
E1V6t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 3)
E1V7t3=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 3)

E1V1t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 4)
E1V2t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 4)
E1V3t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 4)
E1V4t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 4)
E1V5t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 4)
E1V6t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 4)
E1V7t4=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 4)

E1V1t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 5)
E1V2t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 5)
E1V3t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 5)
E1V4t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 5)
E1V5t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 5)
E1V6t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 5)
E1V7t5=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 5)

E1V1t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 6)
E1V2t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 6)
E1V3t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 6)
E1V4t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 6)
E1V5t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 6)
E1V6t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 6)
E1V7t6=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 6)

E1V1t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 7)
E1V2t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 7)
E1V3t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 7)
E1V4t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 7)
E1V5t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 7)
E1V6t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 7)
E1V7t7=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 7)

E1V1t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 8)
E1V2t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 8)
E1V3t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 8)
E1V4t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 8)
E1V5t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 8)
E1V6t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 8)
E1V7t8=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 8)

E1V1t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 9)
E1V2t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 9)
E1V3t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 9)
E1V4t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 9)
E1V5t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 9)
E1V6t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 9)
E1V7t9=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 9)

E1V1t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 10)
E1V2t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 10)
E1V3t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 10)
E1V4t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 10)
E1V5t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 10)
E1V6t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 10)
E1V7t10=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 10)

E1V1t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 11)
E1V2t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 11)
E1V3t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 11)
E1V4t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 11)
E1V5t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 11)
E1V6t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 11)
E1V7t11=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 11)

E1V1t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 12)
E1V2t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 12)
E1V3t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 12)
E1V4t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 12)
E1V5t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 12)
E1V6t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 12)
E1V7t12=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 12)

E1V1t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 13)
E1V2t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 13)
E1V3t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 13)
E1V4t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 13)
E1V5t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 13)
E1V6t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 13)
E1V7t13=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 13)

E1V1t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 14)
E1V2t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 14)
E1V3t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 14)
E1V4t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 14)
E1V5t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 14)
E1V6t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 14)
E1V7t14=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 14)

E1V1t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 15)
E1V2t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 15)
E1V3t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 15)
E1V4t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 15)
E1V5t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 15)
E1V6t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 15)
E1V7t15=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 15)

E1V1t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:1, period_id: 16)
E1V2t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:2, period_id: 16)
E1V3t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:3, period_id: 16)
E1V4t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:4, period_id: 16)
E1V5t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:5, period_id: 16)
E1V6t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:6, period_id: 16)
E1V7t16=EventJobPeriodAssociation.create!(event_id: 1, job_id:7, period_id: 16)

### Für Event 2
E2V1t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 1)
E2V2t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 1)
E2V3t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 1)
E2V4t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 1)
E2V5t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 1)
E2V6t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 1)
E2V7t1=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 1)

E2V1t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 2)
E2V2t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 2)
E2V3t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 2)
E2V4t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 2)
E2V5t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 2)
E2V6t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 2)
E2V7t2=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 2)

E2V1t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 3)
E2V2t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 3)
E2V3t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 3)
E2V4t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 3)
E2V5t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 3)
E2V6t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 3)
E2V7t3=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 3)

E2V1t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 4)
E2V2t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 4)
E2V3t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 4)
E2V4t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 4)
E2V5t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 4)
E2V6t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 4)
E2V7t4=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 4)

E2V1t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 5)
E2V2t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 5)
E2V3t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 5)
E2V4t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 5)
E2V5t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 5)
E2V6t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 5)
E2V7t5=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 5)

E2V1t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 6)
E2V2t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 6)
E2V3t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 6)
E2V4t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 6)
E2V5t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 6)
E2V6t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 6)
E2V7t6=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 6)

E2V1t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 7)
E2V2t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 7)
E2V3t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 7)
E2V4t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 7)
E2V5t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 7)
E2V6t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 7)
E2V7t7=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 7)

E2V1t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 8)
E2V2t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 8)
E2V3t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 8)
E2V4t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 8)
E2V5t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 8)
E2V6t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 8)
E2V7t8=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 8)

E2V1t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 9)
E2V2t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 9)
E2V3t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 9)
E2V4t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 9)
E2V5t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 9)
E2V6t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 9)
E2V7t9=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 9)

E2V1t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 10)
E2V2t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 10)
E2V3t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 10)
E2V4t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 10)
E2V5t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 10)
E2V6t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 10)
E2V7t10=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 10)

E2V1t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 11)
E2V2t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 11)
E2V3t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 11)
E2V4t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 11)
E2V5t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 11)
E2V6t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 11)
E2V7t11=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 11)

E2V1t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 12)
E2V2t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 12)
E2V3t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 12)
E2V4t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 12)
E2V5t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 12)
E2V6t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 12)
E2V7t12=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 12)

E2V1t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 13)
E2V2t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 13)
E2V3t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 13)
E2V4t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 13)
E2V5t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 13)
E2V6t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 13)
E2V7t13=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 13)

E2V1t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 14)
E2V2t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 14)
E2V3t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 14)
E2V4t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 14)
E2V5t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 14)
E2V6t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 14)
E2V7t14=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 14)

E2V1t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 15)
E2V2t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 15)
E2V3t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 15)
E2V4t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 15)
E2V5t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 15)
E2V6t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 15)
E2V7t15=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 15)

E2V1t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:1, period_id: 16)
E2V2t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:2, period_id: 16)
E2V3t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:3, period_id: 16)
E2V4t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:4, period_id: 16)
E2V5t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:5, period_id: 16)
E2V6t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:6, period_id: 16)
E2V7t16=EventJobPeriodAssociation.create!(event_id: 2, job_id:7, period_id: 16)




### Für Event 3
E3V1t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 1)
E3V2t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 1)
E3V3t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 1)
E3V4t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 1)
E3V5t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 1)
E3V6t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 1)
E3V7t1=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 1)

E3V1t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 2)
E3V2t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 2)
E3V3t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 2)
E3V4t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 2)
E3V5t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 2)
E3V6t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 2)
E3V7t2=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 2)

E3V1t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 3)
E3V2t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 3)
E3V3t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 3)
E3V4t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 3)
E3V5t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 3)
E3V6t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 3)
E3V7t3=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 3)

E3V1t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 4)
E3V2t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 4)
E3V3t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 4)
E3V4t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 4)
E3V5t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 4)
E3V6t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 4)
E3V7t4=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 4)

E3V1t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 5)
E3V2t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 5)
E3V3t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 5)
E3V4t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 5)
E3V5t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 5)
E3V6t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 5)
E3V7t5=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 5)

E3V1t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 6)
E3V2t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 6)
E3V3t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 6)
E3V4t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 6)
E3V5t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 6)
E3V6t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 6)
E3V7t6=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 6)

E3V1t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 7)
E3V2t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 7)
E3V3t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 7)
E3V4t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 7)
E3V5t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 7)
E3V6t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 7)
E3V7t7=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 7)

E3V1t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 8)
E3V2t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 8)
E3V3t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 8)
E3V4t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 8)
E3V5t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 8)
E3V6t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 8)
E3V7t8=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 8)

E3V1t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 9)
E3V2t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 9)
E3V3t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 9)
E3V4t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 9)
E3V5t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 9)
E3V6t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 9)
E3V7t9=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 9)

E3V1t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 10)
E3V2t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 10)
E3V3t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 10)
E3V4t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 10)
E3V5t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 10)
E3V6t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 10)
E3V7t10=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 10)

E3V1t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 11)
E3V2t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 11)
E3V3t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 11)
E3V4t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 11)
E3V5t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 11)
E3V6t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 11)
E3V7t11=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 11)

E3V1t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 12)
E3V2t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 12)
E3V3t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 12)
E3V4t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 12)
E3V5t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 12)
E3V6t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 12)
E3V7t12=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 12)

E3V1t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 13)
E3V2t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 13)
E3V3t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 13)
E3V4t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 13)
E3V5t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 13)
E3V6t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 13)
E3V7t13=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 13)

E3V1t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 14)
E3V2t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 14)
E3V3t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 14)
E3V4t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 14)
E3V5t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 14)
E3V6t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 14)
E3V7t14=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 14)

E3V1t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 15)
E3V2t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 15)
E3V3t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 15)
E3V4t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 15)
E3V5t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 15)
E3V6t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 15)
E3V7t15=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 15)

E3V1t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:1, period_id: 16)
E3V2t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:2, period_id: 16)
E3V3t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:3, period_id: 16)
E3V4t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:4, period_id: 16)
E3V5t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:5, period_id: 16)
E3V6t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:6, period_id: 16)
E3V7t16=EventJobPeriodAssociation.create!(event_id: 3, job_id:7, period_id: 16)







