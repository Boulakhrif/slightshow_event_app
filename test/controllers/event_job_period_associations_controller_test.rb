require 'test_helper'

class EventJobPeriodAssociationsControllerTest < ActionController::TestCase
  setup do
    @event_job_period_association = event_job_period_associations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:event_job_period_associations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create event_job_period_association" do
    assert_difference('EventJobPeriodAssociation.count') do
      post :create, event_job_period_association: { event_id: @event_job_period_association.event_id, job_end: @event_job_period_association.job_end, job_id: @event_job_period_association.job_id, period_id: @event_job_period_association.period_id }
    end

    assert_redirected_to event_job_period_association_path(assigns(:event_job_period_association))
  end

  test "should show event_job_period_association" do
    get :show, id: @event_job_period_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @event_job_period_association
    assert_response :success
  end

  test "should update event_job_period_association" do
    patch :update, id: @event_job_period_association, event_job_period_association: { event_id: @event_job_period_association.event_id, job_end: @event_job_period_association.job_end, job_id: @event_job_period_association.job_id, period_id: @event_job_period_association.period_id }
    assert_redirected_to event_job_period_association_path(assigns(:event_job_period_association))
  end

  test "should destroy event_job_period_association" do
    assert_difference('EventJobPeriodAssociation.count', -1) do
      delete :destroy, id: @event_job_period_association
    end

    assert_redirected_to event_job_period_associations_path
  end
end
